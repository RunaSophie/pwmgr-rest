<?php
// enable to display all errors
error_reporting(E_ALL);

// Checks the Master login credentials
require __DIR__ . '/api_functions/masterPwCheck.php';

// Depending on request method, execute different database interactions
$reqmethod = $_SERVER['REQUEST_METHOD'];

switch ($reqmethod) {
    case 'GET':
        // List credentials
        require __DIR__ . '/api_functions/cred_get.php';
        break;
        
    case 'POST':
        // Add a new credential
        require __DIR__ . '/api_functions/cred_create.php';
        break;
        
    case 'PUT':
        // Edit an existing credential
        require __DIR__ . '/api_functions/cred_update.php';
        break;
        
    case 'DELETE':
        // Permanently delete a credential
        require __DIR__ . '/api_functions/cred_delete.php';
        break;
        
    default:
        // Unknown Request Method
        $body = [
            'code' => 405,
            'error' => 'INVALID_METHOD',
            'message' => 'Only Methods allowed: GET, POST, PUT, DELETE'
        ];
        
        http_response_code($body['code']);
        echo json_encode($body);
        die;
        break;
}
?>

