<?php
namespace api_functions;

// double check the database connection
if (!isset($pdo) || !isset($userId)) {
    $body = [
        'code' => 500,
        'error' => 'NO_DATABASE_CONNECTION',
        'message' => 'Database connection unset'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Check if ID is set
if (!isset($_GET['id'])) {
    $body = [
        'code' => 400,
        'error' => 'ID_NOT_SET',
        'message' => 'ID of updated entry not set'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Check if ID exists and belongs to that user
$credId = $_GET['id'];
$stmt = $pdo->prepare('SELECT * FROM mgrcredential WHERE id = :id AND userId = :uid');
$stmt->execute([':id' => $credId, ':uid' => $userId]);

if ($stmt->rowCount() == 0) {
    $body = [
        'code' => 404,
        'error' => 'ID_NOT_FOUND',
        'message' => 'A credential with this ID could not be found for this user'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Only proceed if content type is in JSON format
$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
if (stripos($content_type, 'application/json') === false) {
    $body = [
        'code' => 415,
        'error' => 'INVALID_CONTENT_TYPE',
        'message' => 'Request Body must be in JSON Format'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}


// Read the input stream
$requestBody = file_get_contents("php://input");

// Decode the JSON object
$decodedBody = json_decode($requestBody, true);

$updateSet = '';
// the WHERE contents are always set
$executeArray = [':id' => $credId, ':uid' => $userId];
$passwordSet = 0;

// parse the decoded object to the SQL updating
foreach ($decodedBody as $key => $val) {
    switch ($key) {
        case 'username':
            if ($updateSet != '') {
                $updateSet .= ', ';
            }
            $updateSet .= 'username = :un';
            $executeArray[':un'] = $val;
            break;
            
        case 'password':
            // the password-to-encrypt can always be added because it overrides the encrypted stuff completely
            if ($updateSet != '') {
                $updateSet .= ', ';
            }
            $updateSet .= 'password = :pw, encryption_iv = :enciv';
            
            $enciv = random_bytes(openssl_cipher_iv_length("AES-256-CBC"));
            $pwenc = openssl_encrypt($val, "AES-256-CBC", md5($password, true), 0, $enciv);
            $enciv = bin2hex($enciv);
            
            $executeArray[':pw'] = $pwenc;
            $executeArray[':enciv'] = $enciv;
            
            $passwordSet = 2;
            break;
            
        case 'encrypted_password':
            // only add this if password hasn't been completely added yet
            if ($passwordSet < 2) {
                if ($updateSet != '') {
                    $updateSet .= ', ';
                }
                $updateSet .= 'password = :pw';
                
                $executeArray[':pw'] = $val;
                
                // 0 -> 1 or 1 -> 2
                // if first step, password can still be set, if second step, mark as completed
                $passwordSet++;
            }
            break;
            
        case 'encryption_iv':
            // only add this if password hasn't been completely added yet
            if ($passwordSet < 2) {
                if ($updateSet != '') {
                    $updateSet .= ', ';
                }
                $updateSet .= 'encryption_iv = :enciv';
                
                $executeArray[':enciv'] = $val;
                
                // 0 -> 1 or 1 -> 2
                // if first step, password can still be set, if second step, mark as completed 
                $passwordSet++;
            }
            break;
            
        case 'domain':
            if ($updateSet != '') {
                $updateSet .= ', ';
            }
            $updateSet .= 'domain = :dom';
            $executeArray[':dom'] = $val;
            break;
            
        case 'description':
            if ($updateSet != '') {
                $updateSet .= ', ';
            }
            $updateSet .= 'bezeichnung = :bez';
            $executeArray[':bez'] = $val;
            break;
    }
}

// nothing to update, therefore do nothing
if ($updateSet == '') {
    $body = [
        'code' => 400,
        'error' => 'INVALID_REQUEST',
        'message' => 'Nothing to update'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// if only either the encrypted password or the encryption iv are set, throw error
if ($passwordSet == 1) {
    $body = [
        'code' => 400,
        'error' => 'INVALID_REQUEST',
        'message' => 'Encrypted Password and Encryption IV both need to be provided'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// execute the actual updating with the prepared statement
$sql = 'UPDATE mgrcredential SET ' . $updateSet . ' WHERE id = :id AND userId = :uid';

$stmt = $pdo->prepare($sql);
$stmt->execute($executeArray);

// select and return the 
$stmt = $pdo->prepare('SELECT * FROM mgrcredential WHERE id = :id AND userId = :uid');
$stmt->execute([':id' => $credId, ':uid' => $userId]);

$result = $stmt->fetch(\PDO::FETCH_ASSOC);
echo json_encode($result);


