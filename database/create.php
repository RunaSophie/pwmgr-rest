<?php
namespace database;

require_once __DIR__ . '/../config/Config.php';

function createTables(\PDO $pdo) {
    
    // Create users table
    $sql = 'CREATE TABLE IF NOT EXISTS mgruser (' .
        'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, '.
        'username VARCHAR(50) NOT NULL UNIQUE, '.
        'email VARCHAR(255) NOT NULL UNIQUE, '.
        'masterpw VARCHAR(255) NOT NULL, '.
        'loginFails INT NOT NULL DEFAULT 0);';
    $pdo->query($sql);
    
    // Create credentials table
    $sql = 'CREATE TABLE IF NOT EXISTS mgrcredential (' .
        'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, '.
        'userid INT NOT NULL, '.
        'username VARCHAR(255) NOT NULL, '.
        'password VARCHAR(255) NOT NULL, '.
        'encryption_iv VARCHAR(64) NOT NULL, '.
        'bezeichnung VARCHAR(100) NULL, '.
        'domain VARCHAR(100) NOT NULL, '.
        'FOREIGN KEY (userid) REFERENCES mgruser(id));';
    $pdo->query($sql);
}