<?php
namespace database;

require_once __DIR__ . '/../config/Config.php';

function connect() : \PDO {
    
    // create a new PDO connection with the saved configuration
    $dbsrv = \config\Config::$dbserver;
    $dbport = \config\Config::$dbport;
    $dbname = \config\Config::$dbname;
    $dsn = "mysql:host=$dbsrv;port=$dbport;dbname=$dbname";
    $username = \config\Config::$dbuser;
    $password = \config\Config::$dbpass;
    
    return new \PDO($dsn, $username, $password);
}
