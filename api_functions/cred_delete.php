<?php
namespace api_functions;

// double check the database connection
if (!isset($pdo) || !isset($userId)) {
    $body = [
        'code' => 500,
        'error' => 'NO_DATABASE_CONNECTION',
        'message' => 'Database connection unset'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Check if ID is set
if (!isset($_GET['id'])) {
    $body = [
        'code' => 400,
        'error' => 'ID_NOT_SET',
        'message' => 'ID of deleted entry not set'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Check if ID exists and belongs to that user
$credId = $_GET['id'];
$stmt = $pdo->prepare('SELECT * FROM mgrcredential WHERE id = :id AND userId = :uid');
$stmt->execute([':id' => $credId, ':uid' => $userId]);

if ($stmt->rowCount() == 0) {
    $body = [
        'code' => 404,
        'error' => 'ID_NOT_FOUND',
        'message' => 'A credential with this ID could not be found for this user'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// execute the actual updating with the prepared statement
$sql = 'DELETE FROM mgrcredential WHERE id = :id AND userId = :uid';

$stmt = $pdo->prepare($sql);
$stmt->execute([':id' => $credId, ':uid' => $userId]);

http_response_code(204);
