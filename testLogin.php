<?php
// Checks the Master login credentials
require __DIR__ . '/api_functions/masterPwCheck.php';

// Success independent on request methods or parameters
$body = [
    'code' => 200,
    'message' => 'Login Successful!'
];

http_response_code($body['code']);
echo json_encode($body);