<?php
namespace config;

final class Config
{
    private function __construct() {}
    
    // Database Server config
    public static $dbserver = 'localhost';
    public static $dbport = 3306;
    public static $dbname = 'pwmgr';
    public static $dbuser = 'root';
    public static $dbpass = null;
}

?>