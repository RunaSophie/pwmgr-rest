<?php
namespace api_functions;

// double check the database connection
if (!isset($pdo) || !isset($userId)) {
    $body = [
        'code' => 500,
        'error' => 'NO_DATABASE_CONNECTION',
        'message' => 'Database connection unset'
    ];

    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

$sql = 'SELECT id, username, password, encryption_iv, bezeichnung, domain FROM mgrcredential';

$sqlWhere = 'userid = :uid';
$sqlOrder = 'bezeichnung';
$preparations = [':uid' => $userId];

// Filter by credential ID
if (isset($_GET['id'])) {
    $sqlWhere .= " AND id = :id";
    $preparations[':id'] = $_GET['id'];
}

// Filter by credential description
if (isset($_GET['desc'])) {
    $sqlWhere .= " AND bezeichnung LIKE CONCAT('%', :bez, '%')";
    $preparations[':bez'] = $_GET['desc'];
}

// Filter by domain name
if (isset($_GET['domain'])) {
    $sqlWhere .= " AND domain LIKE CONCAT('%', :dom, '%')";
    $preparations[':dom'] = $_GET['domain'];
}

// Sorting
if (isset($_GET['sortBy'])) {
    switch ($_GET['sortBy']) {
        case 'username':
            $sqlOrder = 'username';
            break;
        case 'id':
            $sqlOrder = 'id';
            break;
        case 'domain':
            $sqlOrder = 'domain';
            break;
    }
}

// Sorting order
if (isset($_GET['order']) && $_GET['order'] == 'desc') {
    $sqlOrder .= ' DESC';
} else {
    $sqlOrder .= ' ASC';
}

// Execute the prepared SQL statement
$stmt = $pdo->prepare($sql . ' WHERE ' . $sqlWhere . ' ORDER BY ' . $sqlOrder);
$stmt->execute($preparations);
$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

if ($stmt->rowCount() > 0 || !isset($_GET['id'])) {
    echo json_encode($result);
} else {
    $body = [
        'code' => 404,
        'error' => 'ID_NOT_FOUND',
        'message' => 'A credential with this ID could not be found for this user'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

?>