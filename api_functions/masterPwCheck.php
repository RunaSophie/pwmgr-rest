<?php
namespace api_functions;

header("Content-Type: application/json; charset=UTF-8");

require_once __DIR__ . '/../database/create.php';
require_once __DIR__ . '/../database/Database.php';

// Create the database connection
try {
    $pdo = \database\connect();

    // if necessary, create non-existing tables in the database
    \database\createTables($pdo);
} catch(\PDOException $ex) {
    $body = [
        'code' => 500,
        'error' => 'DB_UNREACHABLE',
        'message' => 'Database server unreachable: ' . $ex->getMessage()
    ];

    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Prepare headers and later used variables
$headers = getallheaders();
$username = null;
$password = null;

foreach ($headers as $key => $value) {

    // Test each header line for use
    switch ($key) {
        case 'Authorization':

            // Only Basic (Username / Password) Authorization supported
            if (strpos($value, 'Basic ') !== 0) {
                $body = [
                    'code' => 401,
                    'error' => 'AUTHORIZATION_INVALID',
                    'message' => 'Invalid authorization provided'
                ];

                http_response_code($body['code']);
                echo json_encode($body);
                die;
            }

            // Decode the rest of the Authorization string
            $basedunpw = substr($value, strlen('Basic '));
            $unpw = base64_decode($basedunpw, true);

            if ($unpw === false) {
                $body = [
                    'code' => 401,
                    'error' => 'AUTHORIZATION_INVALID',
                    'message' => 'Invalid authorization provided'
                ];

                http_response_code($body['code']);
                echo json_encode($body);
                die;
            }

            // separate the username and password
            // in basic authorization, they are separated by a ':' symbol
            $unpwarr = explode(':', $unpw, 2);
            $username = $unpwarr[0];
            $password = $unpwarr[1];
            break;

        default:
            // unknown headers will not be processed
            break;
    }
}

// If the authorization is not or incorrectly provided, username or password will not have been set
if (!isset($username) || !isset($password)) {
    $body = [
        'code' => 401,
        'error' => 'NO_AUTHORIZATION',
        'message' => 'No authorization provided'
    ];

    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// try to select the user with the given username
$stmt = $pdo->prepare('SELECT id, masterpw, loginFails FROM mgruser WHERE (username = :un OR email = :em)');
$stmt->execute([':em' => $username, ':un' => $username]);

if ($stmt->rowCount() > 0) {
    // only fetch if username exists
    $result = $stmt->fetch(\PDO::FETCH_ASSOC);
    $userId = $result['id'];

    // check how many times the user has tried to log in with a wrong password
    if ($result["loginFails"] < 3) {

        // check whether the given password matches the saved hash
        if (password_verify($password, $result['masterpw'])) {
            // reset the login failure counter
            $sql = "UPDATE mgruser SET loginFails = 0 WHERE id = :id";
            $stmt2 = $pdo->prepare($sql);
            $stmt2->execute([":id" => $userId]);

        } else {
            // increment login failures
            $sql = "UPDATE mgruser SET loginFails = loginFails + 1 WHERE id = :id";
            $stmt2 = $pdo->prepare($sql);
            $stmt2->execute([":id" => $userId]);

            $body = [
                'code' => 401,
                'error' => 'CREDENTIALS_INVALID',
                'message' => 'Username or Password invalid'
            ];

            http_response_code($body['code']);
            echo json_encode($body);
            die;
        }
    } else {
        // user is locked (tried to log in 3 times with wrong password)
        $body = [
            'code' => 401,
            'error' => 'USER_LOCKED',
            'message' => 'User locked. Please contact the administrator.'
        ];

        http_response_code($body['code']);
        echo json_encode($body);
        die;
    }
} else {
    $body = [
        'code' => 401,
        'error' => 'CREDENTIALS_INVALID',
        'message' => 'Username or Password invalid'
    ];

    http_response_code($body['code']);
    echo json_encode($body);
    die;
}
 ?>