<?php
namespace api_functions;

// double check the database connection
if (!isset($pdo) || !isset($userId)) {
    $body = [
        'code' => 500,
        'error' => 'NO_DATABASE_CONNECTION',
        'message' => 'Database connection unset'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

// Only proceed if content type is in JSON format
$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
if (stripos($content_type, 'application/json') === false) {
    $body = [
        'code' => 415,
        'error' => 'INVALID_CONTENT_TYPE',
        'message' => 'Request Body must be in JSON Format'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}


// Read the input stream
$requestBody = file_get_contents("php://input");

// Decode the JSON object
$decodedBody = json_decode($requestBody, true);

// Only continue if all necessary parameters are set
if (!isset($decodedBody['username']) ||
    !isset($decodedBody['domain'])) {

    $body = [
        'code' => 400,
        'error' => 'INVALID_REQUEST',
        'message' => 'Not all necessary parameters set!'
    ];

    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

if (isset($decodedBody['encrypted_password']) && isset($decodedBody['encryption_iv'])) {
    $pwenc = $decodedBody['encrypted_password'];
    $enciv = $decodedBody['encryption_iv'];
} else if (isset($decodedBody['password'])) {
    $enciv = random_bytes(openssl_cipher_iv_length("AES-256-CBC"));
    $pwenc = openssl_encrypt($decodedBody['password'], "AES-256-CBC", md5($password, true), 0, $enciv);
    $enciv = bin2hex($enciv);
} else {
    $body = [
        'code' => 400,
        'error' => 'INVALID_REQUEST',
        'message' => 'Not all necessary parameters set!'
    ];
    
    http_response_code($body['code']);
    echo json_encode($body);
    die;
}

$sql = 'INSERT INTO mgrcredential (userid, username, password, encryption_iv, bezeichnung, domain) '.
       'VALUES (:uid, :un, :pw, :enciv, :bez, :dom)';
$stmt = $pdo->prepare($sql);

$values = [
    ':uid' => $userId,
    ':un' => $decodedBody['username'],
    ':pw' => $pwenc,
    ':enciv' => $enciv,
    ':bez' => $decodedBody['description'],
    ':dom' => $decodedBody['domain']
];

$stmt->execute($values);

$sql = 'SELECT id, username, password, encryption_iv, bezeichnung, domain FROM mgrcredential WHERE id = LAST_INSERT_ID()';
$stmt = $pdo->prepare($sql);
$stmt->execute();

$result = $stmt->fetch(\PDO::FETCH_ASSOC);

echo json_encode($result);


